/**
 * Created by macblady on 12.12.2017.
 */
public interface TemplateBuilder {
    HTMLTemplate build();
    void setJsonConfPath(String path);
    void setHtmlTemplatePath(String path);
}
