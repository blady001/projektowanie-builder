/**
 * Created by macblady on 12.12.2017.
 */
public class BuilderTest {
    public static void main(String[] args) {
        TemplateBuilder builder = new HTMLTemplateBuilder();
        builder.setHtmlTemplatePath("sample.html");
        builder.setJsonConfPath("templateConf.json");
        HTMLTemplate template = builder.build();
        System.out.println(template.returnHTML());
    }
}
