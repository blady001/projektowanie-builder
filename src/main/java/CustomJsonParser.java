import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

/**
 * Created by macblady on 12.12.2017.
 */
public class CustomJsonParser {
    private String filename;
    private JSONObject jobj;

    private CustomJsonParser(String filename) throws ParseException, IOException {
        this.filename = filename;
        this.jobj = this.readJson(filename);
    }

    public int getInt(String key) {
        return (Integer) this.jobj.get(key);
    }

    public String getString(String key) {
        return (String) this.jobj.get(key);
    }

    public static CustomJsonParser parse(String filename) throws ParseException, IOException {
        return new CustomJsonParser(filename);
    }

    private JSONObject readJson(String filename) throws ParseException, IOException {
        JSONParser parser = new JSONParser();
        FileReader configFile = null;
        String jsonConfig = "";

        try {
            configFile = new FileReader(filename);
            JSONObject obj = (JSONObject) parser.parse(configFile);
            return obj;
        } finally {
            configFile.close();
        }
    }
}
