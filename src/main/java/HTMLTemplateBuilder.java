import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by macblady on 12.12.2017.
 */
public class HTMLTemplateBuilder implements TemplateBuilder {
    private String jsonConfPath;
    private String htmlTemplatePath;

    public HTMLTemplateBuilder() {

    }

    public void setJsonConfPath(String path) {
        this.jsonConfPath = path;
    }

    public void setHtmlTemplatePath(String path) {
        this.htmlTemplatePath = path;
    }

    private String readTemplate() throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(this.htmlTemplatePath));
        return new String(encoded, StandardCharsets.UTF_8);
    }

    private Map<String, String> createVariableMapping(List<String> variableNames, CustomJsonParser parser) {
        Map<String, String> mapping = new HashMap<>();
        for (String var : variableNames) {
            String value = parser.getString(var);
            mapping.put(var, value);
        }
        return mapping;
    }


    public HTMLTemplate build() {
        String htmlStr = "";
        try {
            String templateStr = this.readTemplate();
            RegexHelper reHelper = new RegexHelper(templateStr);
            List<String> variables = reHelper.getAllMatches();
            CustomJsonParser parser = CustomJsonParser.parse(this.jsonConfPath);
            Map<String, String> replacement = this.createVariableMapping(variables, parser);
            htmlStr = reHelper.getReplacedContent(replacement);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new HTMLTemplate(htmlStr);
    }
}
